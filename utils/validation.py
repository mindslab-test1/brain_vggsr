import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F
from scipy.optimize import brentq
from sklearn.metrics import roc_curve
from scipy.interpolate import interp1d


def get_cluster_kg(hp, x_k_center):
    A = F.softmax(x_k_center, dim=1)
    A = A.view(hp.model.cluster.K+hp.model.cluster.G, -1)
    A = A.transpose(0, 1)
    k_values = torch.sum(A[:, :hp.model.cluster.K], dim=1).tolist()
    g_values = torch.sum(A[:, hp.model.cluster.K:], dim=1).tolist()
    return k_values, g_values


def validate(hp, model, testloader, writer, step):
    model.eval()
    torch.backends.cudnn.benchmark = False

    cluster_k = hp.model.cluster.K
    cluster_g = hp.model.cluster.G

    with torch.no_grad():
        gt = list()
        scores = list()
        k_averages = list()
        g_averages = list()

        first_file = False
        for target, audio1, audio2 in tqdm.tqdm(testloader):
            target = target.cuda(); audio1 = audio1.cuda(); audio2 = audio2.cuda()
            output1, x_k_center_1 = model.inference(audio1)
            output2, x_k_center_2 = model.inference(audio2)
            similarity = F.cosine_similarity(output1, output2)
            gt.append(target)
            scores.append(similarity)

            k1, g1 = get_cluster_kg(hp, x_k_center_1)
            k2, g2 = get_cluster_kg(hp, x_k_center_2)
            k_averages.append(sum(k1)/len(k1))
            g_averages.append(sum(g1)/len(g1))
            k_averages.append(sum(k2)/len(k2))
            g_averages.append(sum(g2)/len(g2))

            if not first_file:
                first_file = True
                spectrogram1 = model.make_spectrogram(audio1)
                spectrogram1 = spectrogram1.cpu().detach().numpy()[0]
                writer.log_cluster_plot(k1, g1, spectrogram1, step)

        gt = torch.cat(gt, dim=0).tolist()
        scores = torch.cat(scores, dim=0).tolist()

        fpr, tpr, thresholds = roc_curve(gt, scores, pos_label=1)
        eer = brentq(lambda x: 1. - x - interp1d(fpr, tpr)(x), 0., 1.)
        thr = interp1d(fpr, thresholds)(eer)

        writer.log_validation(fpr, tpr, thresholds, eer, thr, step)

        k_avg = sum(k_averages) / len(k_averages)
        g_avg = sum(g_averages) / len(g_averages)
        writer.log_cluster_values(k_avg, g_avg, step)

    model.train()
    torch.backends.cudnn.benchmark = True
