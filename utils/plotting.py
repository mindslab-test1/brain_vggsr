import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np


def save_figure_to_numpy(fig):
    # save it to a numpy array.
    data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
    data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    data = data.transpose(2, 0, 1)
    return data

def plot_far_frr_to_numpy(thresholds, far, frr):
    fig, ax = plt.subplots(figsize=(4, 4))
    ax.plot(thresholds, far, alpha=0.5, color='green', linestyle='--', label='FAR')
    ax.plot(thresholds, frr, alpha=0.5, color='red', label='FRR')

    plt.legend(loc='upper right')

    plt.xlabel("Threshold")
    plt.xlim(0, 1)
    plt.ylabel("Probability")
    plt.ylim(0, 1)
    plt.tight_layout()

    fig.canvas.draw()
    data = save_figure_to_numpy(fig)
    plt.close()
    return data

def plot_roc_to_numpy(far, tpr):
    fig, ax = plt.subplots(figsize=(4, 4))
    ax.plot(far, tpr, color='red')

    plt.xlabel("FAR")
    plt.xlim(0, 1)
    plt.ylabel("TPR")
    plt.ylim(0, 1)
    plt.tight_layout()

    fig.canvas.draw()
    data = save_figure_to_numpy(fig)
    plt.close()
    return data

def plot_vlad_values(k_values, g_values, spectrogram):
    ind = np.arange(len(k_values))
    width = 0.35

    fig = plt.figure(figsize=(12, 7))

    # plot weights for K/G cluster
    plt.subplot(211)
    plt.bar(ind, k_values, width, label='K clusters')
    plt.bar(ind, g_values, width, bottom=k_values, label='G clusters')
    plt.ylabel('Scores')
    plt.ylim(0, 1)
    plt.legend(loc=3)
    plt.subplots_adjust(bottom=0.1, right=0.88, top=0.9)

    # plot corresponding spectrogram
    plt.subplot(212)
    plt.imshow(spectrogram, aspect='auto', origin='lower', interpolation='none')
    plt.xlabel('Time frames')
    plt.subplots_adjust(bottom=0.1, right=0.88, top=0.9)

    cax = plt.axes([0.9, 0.1, 0.02, 0.8])
    plt.colorbar(cax=cax)

    fig.canvas.draw()
    data = save_figure_to_numpy(fig)
    plt.close()
    return data
