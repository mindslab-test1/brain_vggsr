FROM nvcr.io/nvidia/pytorch:20.03-py3
RUN python3 -m pip --no-cache-dir install --upgrade \
        omegaconf==2.0.0 \
        gpustat \
        grpcio==1.13.0 \
        grpcio-tools==1.13.0 \
        protobuf==3.5.1 \
        && \
apt update && \
apt install -y \
    tmux \
    htop \
    ncdu \
    git-lfs \
    && \
git lfs install 
RUN mkdir /root/ghostvlad
COPY . /root/ghostvlad
RUN cd /root/ghostvlad/ && \
    python3 -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. dap.proto
