import argparse
import grpc

from dap_pb2 import WavBinary, Embedding
from dap_pb2_grpc import GhostVLADStub


class GhostVLADClient(object):
    def __init__(self, remote='127.0.0.1:43001', chunk_size=3145728):
        channel = grpc.insecure_channel(remote)
        self.stub = GhostVLADStub(channel)
        self.chunk_size = chunk_size

    def get_dvector_from_wav(self, wav_binary):
        wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.GetDvectorFromWav(wav_binary)

    def get_emb_config(self):
        return self.stub.GetEmbConfig(empty.Empty())

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield WavBinary(bin=wav_binary[idx:idx+self.chunk_size])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='ghostvlad client')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='grpc: ip:port',
                        type=str,
                        default='127.0.0.1:43001')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='input wav file',
                        type=str,
                        required=True)
    args = parser.parse_args()

    client = GhostVLADClient(args.remote)

    with open(args.input, 'rb') as rf:
        wav_binary = rf.read()
    
    dvector_result = client.get_dvector_from_wav(wav_binary).data
    print(dvector_result)
