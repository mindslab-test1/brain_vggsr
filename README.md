# vggsr (a.k.a. GhostVLAD)

PyTorch implementation of https://arxiv.org/abs/1902.10107.
- Input: raw audio
- Output: 512-dimension speaker vector

README.md WIP

## requirements

`requirements.txt`, `git-lfs`

or

`Dockerfile`

## Build

```bash
docker build -f Dockerfile -t ghostvlad:<version> .
docker build -f Dockerfile-server --build-arg ghostvlad_version=<version> -t ghostvlad_server:<version> .
```


## Inference (Serving)

```bash
docker run --gpus '"device=0"' -d -v /raid/swpark/vggsr/chkpt:/model -p 43001:43001 -e GHOSTVLAD_YAML=/model/<path_to_config_yaml> -e GHOSTVLAD_MODEL=/model/<path_to_checkpoint> ghostvlad_server:<version>
```

## Training

### Download data

VoxCeleb2-train, VoxCeleb1-test. You need to download it from http://www.robots.ox.ac.uk/~vgg/data/voxceleb/vox2.html.
Use the username `voxceleb1902` and password `nx0bl2v2`.
Alternatively, you may `scp` from another server; consult the author if you need one.

### Pull Metadata

```bash
git lfs pull
```

### Change file format (or resample)

The original VoxCeleb2-train is consisted of `.m4a` files.
You need to change them into `.wav` using ffmpeg:

```bash
sh reformat_delete.sh
```

## Miscellaneous

### Things that are inconsistent with paper
- used `m=0.35` for AMS-Softmax, instead of `m=0.4`
- Thin ResNet's `-2`th layer have stride `(2, 1)`, instead of `(2, 2)`


## Author

브레인 박승원 선임
