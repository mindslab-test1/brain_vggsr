import logging
import time
import torch
import grpc
import argparse
import librosa
import numpy as np

from scipy.io.wavfile import read
from concurrent import futures
from omegaconf import OmegaConf


from model.model import VGGSR
from utils.wav_wrapper import WavBinaryWrapper
from utils.utils import wav_formatter

import dap_pb2
from dap_pb2_grpc import GhostVLADServicer, add_GhostVLADServicer_to_server

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class GhostVLADInference(GhostVLADServicer):
    def __init__(self, checkpoint_path, device, config_path):
        super().__init__()
        torch.cuda.set_device(device)
        self.device = device

        checkpoint = torch.load(checkpoint_path, map_location='cpu')
        hp_chkpt = OmegaConf.create(checkpoint['hp_str'])
        hp = OmegaConf.load(config_path)
        self.hp = hp
        assert hp_chkpt.audio == hp.audio, \
            'HParams of checkpoint and config_path do not match. Expected:\n%s\nGot:\n%s\n' % \
            (hp_chkpt.audio.pretty(), hp.audio.pretty())

        self.model = VGGSR(hp).to(device)
        self.model.load_state_dict(checkpoint['model'])
        self.model.eval()

        self.emb_config = dap_pb2.EmbConfig(
            emb_dim=hp.model.dim
        )

        del checkpoint 
        torch.cuda.empty_cache()

    @torch.no_grad()
    def GetDvectorFromWav(self, wav_binary_iterator, context):
        torch.cuda.set_device(self.device)
        try:
            wav = bytearray()
            for wav_binary in wav_binary_iterator:
                wav.extend(wav_binary.bin)
            wav = WavBinaryWrapper(wav)

            sampling_rate, wav = read(wav)
            assert sampling_rate == self.hp.audio.sr, \
                'Sampling rate mismatch: expected %d, got %d' % (self.hp.audio.sr, sampling_rate)
            
            wav = wav_formatter(wav)
            
            wav = torch.from_numpy(wav).cuda()
            wav = wav.unsqueeze(0)
            result, _ = self.model.inference(wav)
            result = result[0]
            result = result.float().view(-1).cpu().detach().tolist()
            result = dap_pb2.Embedding(data=result)
            return result
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def GetEmbConfig(self, empty, context):
        return self.emb_config


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='vggsr(ghostvlad) runner executor')
    parser.add_argument('-c', '--config',
                        nargs='?',
                        dest='config',
                        required=True,
                        help='yaml file for configuration',
                        type=str)
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model Path.',
                        type=str)
    parser.add_argument('-l', '--log_level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=43001)
    parser.add_argument('-d', '--device',
                        nargs='?',
                        dest='device',
                        help='gpu device',
                        type=int,
                        default=0)
    parser.add_argument('-w', '--max_workers',
                        nargs='?',
                        dest='max_workers',
                        help='max workers',
                        type=int,
                        default=1)

    args = parser.parse_args()

    vggsr = GhostVLADInference(args.model, args.device, args.config)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.max_workers), )
    add_GhostVLADServicer_to_server(vggsr, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('vggsr(ghostvlad) starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
