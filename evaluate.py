import os
import glob
import torch
import argparse
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read
from omegaconf import OmegaConf

from utils.utils import cosine_similarity, wav_formatter
from model.model import VGGSR


def main(hp, args):
    model = VGGSR(hp).cuda()
    checkpoint = torch.load(args.checkpoint_path)
    model.load_state_dict(checkpoint['model'])
    model.eval()

    embeddings = list()

    wavlist = glob.glob(os.path.join(args.input_path, '*.wav'))
    with torch.no_grad():
        for wavpath in wavlist:
            print(wavpath)
            sr, wav = read(wavpath)
            assert sr == hp.audio.sr

            wav = wav_formatter(wav)
            wav = torch.from_numpy(wav).cuda()
            wav = wav.unsqueeze(0)
            result, _ = model.inference(wav)
            result = result[0]
            embeddings.append(result.cpu())

    embeddings = torch.stack(embeddings).cuda()
    similarity = cosine_similarity(embeddings, embeddings)
    similarity = similarity.cpu().numpy()

    fig, ax = plt.subplots(figsize=(6, 6))
    im = ax.imshow(similarity, aspect='auto', origin='lower',
                   interpolation='none')
    for i in range(len(wavlist)):
        for j in range(len(wavlist)):
            text = ax.text(j, i, '%.02f' % similarity[i, j],
                            ha="center", va="center", color="w")
    
    ax.set_xticks(np.arange(len(wavlist)))
    ax.set_yticks(np.arange(len(wavlist)))
    ax.set_xticklabels([x.split('\\')[-1][:-4] for x in wavlist])
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
    ax.set_yticklabels([x.split('\\')[-1][:-4] for x in wavlist])
    plt.title(args.checkpoint_path)
    plt.show()

if __name__ == '__main__':	
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="yaml file for config.")
    parser.add_argument('-p', '--checkpoint_path', required=True,
                        help="path of checkpoint pt file for evaluation")
    parser.add_argument('-i', '--input_path', type=str, default=None,
    					help="path of utterances to compare")
    args = parser.parse_args()
    hp = OmegaConf.load(args.config)

    main(hp, args)
