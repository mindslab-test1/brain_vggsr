import torch
import torch.nn as nn
import torch.nn.functional as F


class Block(nn.Module):
    def __init__(self, kernel, channels, stride=1, identity=True):
        super(Block, self).__init__()
        self.identity = identity
        self.conv = nn.Sequential(
            nn.Conv2d(channels[0], channels[1], 1, stride=stride, bias=False),
            nn.BatchNorm2d(channels[1]),
            nn.ReLU(),
            nn.Conv2d(channels[1], channels[2], kernel_size=kernel, padding=(kernel//2), bias=False),
            nn.BatchNorm2d(channels[2]),
            nn.ReLU(),
            nn.Conv2d(channels[2], channels[3], 1, bias=False),
            nn.BatchNorm2d(channels[3])
        )

        if not identity:
            self.residual = nn.Sequential(
                nn.Conv2d(channels[0], channels[3], 1, stride=stride, bias=False),
                nn.BatchNorm2d(channels[3])
            )

    def forward(self, x):
        skip = x if self.identity else self.residual(x)
        x = self.conv(x)
        x = x + skip
        x = F.relu(x)
        return x
